

// BÀI 1: XẾP SỐ
//
function xepSo() {
    var a = document.getElementById("num1").value * 1;
    var b = document.getElementById("num2").value * 1;
    var c = document.getElementById("num3").value * 1;

    if (b < a && a < c) {
        document.getElementById("txtNum").innerHTML = `${b} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${a} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${c}`;
    } else if (b < c && c < a) {
        document.getElementById("txtNum").innerHTML = `${b} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${c} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${a}`;
    } else if (a < c && c < b) {
        document.getElementById("txtNum").innerHTML = `${a} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${c} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${b}`;
    } else if (a < b && b < c) {
        document.getElementById("txtNum").innerHTML = `${a} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${b} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${c}`;
    } else if (c < b && b < a) {
        document.getElementById("txtNum").innerHTML = `${c} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${b} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${a}`;
    } else {
        document.getElementById("txtNum").innerHTML = `${c} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${a} <i class="fa fa-arrow-right" aria-hidden="true"></i> ${b}`;
    }

}

// BÀI 2: HIỆN LỜI CHÀO
function chao() {
    var a = document.getElementById('txt-nguoi').value;
    switch (a) {
        case 'b':
            document.getElementById("txtChao").innerHTML = `<h2 class="text-center display-6">Xin chào bố!</h2>  `
            break;
        case 'm':
            document.getElementById("txtChao").innerHTML = `<h2 class="text-center display-6">Chào mẹ!</h2>  `
            break;
        case 'a':
            document.getElementById("txtChao").innerHTML = `<h2 class="text-center display-6">Xin chào anh trai!</h2>  `
            break;
        case 'e':
            document.getElementById("txtChao").innerHTML = `<h2 class="text-center display-6">Xin chào em gái!</h2>  `
            break;
        default:
            break;
    }

}

// BÀI 3: ĐẾM SỐ CHẴN, LẺ
function demSo() {
    var a = document.getElementById("so1").value * 1;
    var b = document.getElementById("so2").value * 1;
    var c = document.getElementById("so3").value * 1;
    var soChan = 0;
    (a % 2) == 0 ? soChan++ : soChan,
        (b % 2) == 0 ? soChan++ : soChan,
        (c % 2) == 0 ? soChan++ : soChan;
    document.getElementById("txtSo").innerHTML = ` Có ${soChan} số chẵn; Có ${3 - soChan} số lẻ;  `
}

// BÀI 4: KIỂM TRA TAM GIÁC
function canhTG() {
    var a = document.getElementById("canh1").value * 1;
    var b = document.getElementById("canh2").value * 1;
    var c = document.getElementById("canh3").value * 1;
    var kieuTG;
    if ((a + b) > c && (a + c) > b && (b + c) > a) {
        (a != b && b != c && a != c) ? kieuTG = "thường" : kieuTG;
        (a == b || b == c || c == a) ? kieuTG = "cân" : kieuTG,
        (a * a == (b * b + c * c) || b * b == (a * a + c * c) || c * c == (a * a + b * b)) ? kieuTG = "vuông " + kieuTG : kieuTG,
        (a == b && a == c) ? kieuTG = "đều" : kieuTG,
        document.getElementById("txt-placeholder").innerHTML = "Đây là tam giác ";
        document.getElementById("txtTG").innerHTML = kieuTG;
    } else {
        document.getElementById("txtTG").innerHTML = ` Đây không phải 3 cạnh của tam giác  `;
        document.getElementById("txt-placeholder").innerHTML = "";
    }
}